#include <stdio.h>
#include "env_info.h"
#include "environment.gen.h"

void print_project_infos() {
    printf("\n");
    printf(
        "Calculator v%i.%i.%i.%i\n", 
        CONF_VERSION_MAJOR, 
        CONF_VERSION_MINOR,
        CONF_VERSION_PATCH,
        CONF_VERSION_TWEAK);
    
    printf("%s\n", CONF_DESCRIPTION);
    printf("%s\n", CONF_HOMEPAGE_URL);
    printf("\n");
}