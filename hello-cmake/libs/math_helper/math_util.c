#include "math_util.h"
#include "math_helper_config.gen.h"
#include <stdio.h>

int subtract(int a, int b) {
    #ifdef DEBUG_OUTPUT
        printf("%i - %i =\n", a, b);    
    #endif

    return a - b;
}

int sum(int a, int b) {    
    #ifdef DEBUG_OUTPUT
        printf("%i + %i =\n", a, b);
    #endif

    return a + b;
}