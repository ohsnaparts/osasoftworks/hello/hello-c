#include <stdio.h>
#include <stdlib.h>
#include "math_util.h"
#include "config.gen.h"

#ifdef USE_LIB_ENV_INFO
    #include "env_info.h"
#endif


int main(int argc, char **argv) {
    #ifdef USE_LIB_ENV_INFO
        print_project_infos();
    #endif

    if ( argc < 4 ) {
        printf("Usage: %s <operation> <left> <right>\n", argv[0]);
        printf(" Operations:\n");
        printf("   - sum\n");
        printf(" Example:\n");
        printf("   - %s sum 152 13", argv[0]);
        printf("     > 165");
        return 1;
    }

    int a = atoi(argv[2]);
    int b = atoi(argv[3]);
    
    int result = sum(a, b);
    printf("a%i", result);
}