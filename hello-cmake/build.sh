if ! which cmake; then
    echo 'Unable to find cmake executable.'
    echo '   sudo apt install cmake'
    exit 1
fi

# init cmake project
cmake .

# configure optionals
# Turn on environment infos
cmake . -DUSE_LIB_ENV_INFO=OFF \
        -DDEBUG_OUTPUT=OFF

# build project
cmake --build .