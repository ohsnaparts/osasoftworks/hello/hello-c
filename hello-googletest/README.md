# Shirt Producer

This application prints shirts in different colors using a shirt producer. This producer is tested using googletest and checked via valgrind memcheck.

## Run Tests

1. **CLion**
   * GoogleTest integration has been tested successfully with CLion. Select the GoogleTest project in the upper right corner and run, or execute the tests directly from within the tests themselves.
2. **CLI**
   * Building and executing tests is summarized by a custom-made script [`run_tests.sh`](scripts/run_tests.sh).

## Run Application

Build using cmake (see [`run_tests.sh`](scripts/run_tests.sh)) and execute the binary in the `/build` directory. Optionally, you
can use valgrind memchecker to test for memory leaks and bugs.

```bash
valgrind --tool=memcheck build/hello_googletest
```

## Visuals

![](images/shirt-producer-test-and-valgrind.gif)