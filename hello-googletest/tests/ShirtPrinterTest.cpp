#include "gtest/gtest.h"
#include <regex>
#include "iostream"
#include "mocks/ShirtProducerMock.h"
#include "../src/ShirtPrinter/ShirtPrinter.h"


const string testShirt = "  _   _\n"
                         " ) `-' (\n"
                         "|-test♥-|\n"
                         "|_______|\n"
                         "|_/   \\_|\n"
                         "|___|___|\n";


TEST(ShirtPrinter, Print_UsesShirtProducerToProduceShirt) {
    // Given/Then
    ShirtProducerMock producer;
    EXPECT_CALL(producer, Produce())
        .Times(1)
        .WillRepeatedly(testing::Return(testShirt));

    ShirtPrinter printer(&producer, &cout);

    // When
    printer.Print(31);
};


TEST(ShirtPrinter, Print_ReturnsShirtInSpecifiedColor) {
    // Given
    stringstream conveyorBelt;
    ShirtProducer producer;
    auto printer = ShirtPrinter(&producer, &conveyorBelt);

    // When
    printer.Print(31);
    auto output = conveyorBelt.str();

    // Then
    ASSERT_GT(output.length(), 0);
    ASSERT_TRUE(regex_search(output, regex("^\033\\[1;31m"))) << "Input text:\n" << output;
    ASSERT_TRUE(regex_search(output, regex("\033\\[1;0m\n$"))) << "Input text:\n" << output;
};