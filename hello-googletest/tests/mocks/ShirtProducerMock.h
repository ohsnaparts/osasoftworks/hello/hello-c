//
// Created by kami on 1/28/22.
//

#ifndef HELLO_GOOGLETEST_SHIRTPRODUCERMOCK_H
#define HELLO_GOOGLETEST_SHIRTPRODUCERMOCK_H

#include "gmock/gmock.h"
#include "../../src/ShirtProducer/IShirtProducer.h"
#include "string"

class ShirtProducerMock : public IShirtProducer {
public:
    MOCK_METHOD(std::string, Produce, (), (override));
};


#endif //HELLO_GOOGLETEST_SHIRTPRODUCERMOCK_H
