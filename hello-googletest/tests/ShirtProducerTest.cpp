//
// Created by kami on 1/26/22.
//

#include "gtest/gtest.h"
#include "string"
#include "../src/ShirtProducer/ShirtProducer.h"

using namespace std;

TEST(HelloTest, Produce_ReturnsShirtOfExpectedLength) {
    // Given
    auto producer = ShirtProducer();

    // When
    auto shirt = producer.Produce();

    // Then
    EXPECT_EQ(shirt.length(), 73);
}

