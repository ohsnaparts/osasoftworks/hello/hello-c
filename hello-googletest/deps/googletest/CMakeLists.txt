cmake_minimum_required(VERSION 3.16)
set(contentName googletest)

include(FetchContent)

FetchContent_Declare(
    ${contentName}
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG        e2239ee6043f73722e7aa812a459f54a28552929
)

# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

FetchContent_MakeAvailable(${contentName})