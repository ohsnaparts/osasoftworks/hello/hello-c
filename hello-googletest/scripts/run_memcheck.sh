#!/bin/bash
SCRIPT_DIR=$(readlink -f $(dirname "${BASH_SOURCE[0]}"))
PROJECT_BASE="$(readlink -f $SCRIPT_DIR/..)"
PROJECT_EXECUTABLE="$PROJECT_BASE/hello_googletest"

if ! which valgrind > /dev/null; then
	echo "Unable to find valgrind in the PATH"
	exit 1
fi

valgrind --tool=memcheck \
		 --leak-check=full \
		 --track-origins=yes \
		 --xtree-memory=full "$PROJECT_EXECUTABLE" 33 31 34