SCRIPT_DIR=$(readlink -f $(dirname "${BASH_SOURCE[0]}"))
PROJECT_BASE="$(readlink -f $SCRIPT_DIR/..)"
SOURCE_DIR="$PROJECT_BASE"
BUILD_DIR="$PROJECT_BASE/build"
TEST_BUILD_DIR="$BUILD_DIR/tests"
TEST_BINARY="$TEST_BUILD_DIR/ProjectTests"

rm -R "$BUILD_DIR"
mkdir -p "$BUILD_DIR"
cmake -S "$SOURCE_DIR" -B "$BUILD_DIR"
make --directory=$BUILD_DIR
$TEST_BINARY