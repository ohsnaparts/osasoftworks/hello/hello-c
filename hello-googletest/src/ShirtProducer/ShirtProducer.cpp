//
// Created by kami on 1/26/22.
//

#include "ShirtProducer.h"

using namespace std;

string ShirtProducer::Produce() {
    string asciiShirt =
            "  ___ ___\n"
            " /| |/|\\| |\\\n"
            "/_| ´ |.` |_\\\n"
            "  |   |.  |\n"
            "  |   |.  |\n"
            "  |___|.__|";
    return asciiShirt;
}

ShirtProducer::~ShirtProducer()  {}


