//
// Created by kami on 1/26/22.
//

#ifndef HELLO_GOOGLETEST_SHIRTPRODUCER_H
#define HELLO_GOOGLETEST_SHIRTPRODUCER_H

#include "IShirtProducer.h"

class ShirtProducer : public IShirtProducer {
public:
    ~ShirtProducer();
    std::string Produce() override;
};


#endif //HELLO_GOOGLETEST_SHIRTPRODUCER_H
