//
// Created by kami on 1/28/22.
//

#ifndef HELLO_GOOGLETEST_ISHIRTPRODUCER_H
#define HELLO_GOOGLETEST_ISHIRTPRODUCER_H

#include "string"

class IShirtProducer {
public:
    virtual ~IShirtProducer() {}
    virtual std::string Produce() = 0;
};


#endif //HELLO_GOOGLETEST_ISHIRTPRODUCER_H
