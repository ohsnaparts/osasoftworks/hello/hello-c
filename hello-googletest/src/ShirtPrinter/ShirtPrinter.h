//
// Created by kami on 1/26/22.
//

#ifndef HELLO_GOOGLETEST_SHIRTPRINTER_H
#define HELLO_GOOGLETEST_SHIRTPRINTER_H
#include "cstddef"
#include "iostream"
#include "../ShirtProducer/ShirtProducer.h"
#include "IShirtPrinter.h"

using namespace std;

class ShirtPrinter : public IShirtPrinter {
private:
    IShirtProducer* _producer;
    ostream* _outputConveyorBelt;
    void PlaceOnOutputConveyorBelt(string shirt);
public:
    ~ShirtPrinter();
    ShirtPrinter(IShirtProducer* producer, ostream* outputConveyorBelt);
    void ListAvailableColors() override;
    void Print(uint8_t colorId) override;
};


#endif //HELLO_GOOGLETEST_SHIRTPRINTER_H
