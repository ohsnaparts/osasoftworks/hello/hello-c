//
// Created by kami on 1/28/22.
//

#ifndef HELLO_GOOGLETEST_ISHIRTPRINTER_H
#define HELLO_GOOGLETEST_ISHIRTPRINTER_H


#include "../ShirtProducer/ShirtProducer.h"

class IShirtPrinter {
public:
    virtual ~IShirtPrinter() = 0;
    virtual void ListAvailableColors() = 0;
    virtual void Print(uint8_t colorId) = 0;
};


#endif //HELLO_GOOGLETEST_ISHIRTPRINTER_H
