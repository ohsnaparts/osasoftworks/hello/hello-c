//
// Created by kami on 1/26/22.
//

#include "ShirtPrinter.h"
#include <iostream>
#include <sstream>
#include "string"


using namespace std;

#define RESET_COLOR_ID 0;


string GetLinuxTerminalColorCode(uint8_t colorId) {
    stringstream stringBuilder;
    stringBuilder << "\033[1;" << to_string(colorId) << "m";
    return stringBuilder.str();
}

ShirtPrinter::ShirtPrinter(IShirtProducer* producer, ostream *outputConveyorBelt)
        : _producer(producer), _outputConveyorBelt(outputConveyorBelt) {}

// Destructors are called automatically in the reverse order of construction (Base classes last).
IShirtPrinter::~IShirtPrinter() { cout << "Destructing ~IShirtPrinter" << endl; }
ShirtPrinter::~ShirtPrinter() { cout << "Destructing ~ShirtPrinter" << endl; }

string ColorizeShirt(string shirt, uint8_t colorId) {
    string colorCode = GetLinuxTerminalColorCode(colorId);
    string resetColorCode = GetLinuxTerminalColorCode(0);

    stringstream stringBuilder;
    stringBuilder << colorCode << shirt << resetColorCode;
    return stringBuilder.str();

}

void ShirtPrinter::PlaceOnOutputConveyorBelt(string shirt) {
    _outputConveyorBelt->write(shirt.c_str(), shirt.length());
    _outputConveyorBelt->write("\n", 1);
    _outputConveyorBelt->flush();
}

void ShirtPrinter::Print(uint8_t colorId) {
    string rawShirt = _producer->Produce();
    string colorizedShirt = ColorizeShirt(rawShirt, colorId);
    this->PlaceOnOutputConveyorBelt(colorizedShirt);
}

void ShirtPrinter::ListAvailableColors() {
    cout <<
         "color        foreground background\n"
         "-----        ---------- ----------\n"
         "default      0          0\n"
         "black        30         40\n"
         "red          31         41\n"
         "green        32         42\n"
         "yellow       33         43\n"
         "blue         34         44\n"
         "magenta      35         45\n"
         "cyan         36         46\n"
         "white        37         47\n" << endl;
}



