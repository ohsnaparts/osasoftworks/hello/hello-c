#include <iostream>
#include "src/ShirtProducer/ShirtProducer.h"
#include "src/ShirtPrinter/ShirtPrinter.h"
#include "src/ShirtPrinter/IShirtPrinter.h"

string promptForProperty(string propertyName) {
    cout << propertyName << ": ";

    string input;
    std::getline(std::cin, input);
    return input;
}

int8_t promptForColorId() {
    string input = promptForProperty("Color id");

    return input.length() > 0
        ? stoi(input)
        : -1;
}

bool isExitConditionMet(int8_t colorId) {
    const int exitCode = -1;
    return colorId == exitCode;
}

void printInteractively(IShirtPrinter* printer) {
    uint8_t colorId;
    do {
        colorId = promptForColorId();
        printer->Print(colorId);
    } while(!isExitConditionMet(colorId));
}

void printFromInputQueue(IShirtPrinter* printer, char *shirtOrders[], int shirtOrderCount) {
    for(int i = 1; i < shirtOrderCount; i++) {
        auto colorId = stoi(shirtOrders[i]);
        printer->Print(colorId);
    }
}

int main(int argc, char *argv[]) {
    auto shirtProducer = ShirtProducer();
    auto shirtPrinter = ShirtPrinter(&shirtProducer, &cout);

    shirtPrinter.ListAvailableColors();

    bool isInteractive = argc == 1;
    if (isInteractive) {
        printInteractively(&shirtPrinter);
    } else {
        printFromInputQueue(&shirtPrinter, argv, argc);
    }

    return 0;
}